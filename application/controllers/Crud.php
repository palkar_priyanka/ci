<?php
    class Crud extends CI_controller{
        
        function __construct() {
            parent::__construct();
            $this->load->model('crud_model');
        }

        public function index(){
            $data['data'] = $this->crud_model->show_all();
            $this->load->view('listing',$data);
        }

        public function create(){
            if(isset($_POST['submit'])){
                $data = array(
                    'name' => $_POST['name'],
                    'address' => $_POST['address'],
                    'gender' => $_POST['gender']
                );
                $result = $this->crud_model->insert_data('users',$data);
                if ($result == true){
                    redirect(base_url('/index.php/crud/index'));
                }else{
                    $this->load->view('registration'); 
                }
            }else{
                $this->load->view('registration'); 
            }   
        }

        public function edit(){
            $id = $_POST['id'];
            $data['data'] = $this->crud_model->user_get_by_id('users',$id);
            $this->load->view('register_update',$data);            
        }

        public function update(){
            $id = $_REQUEST['id'];
            if($_POST['submit'] == 'update'){
                $data = array(
                    'name' => $_POST['name'],
                    'address' => $_POST['address'],
                    'gender' => $_POST['gender']
                );
                $this->crud_model->update('users',$id,$data);
                redirect(base_url('index.php/crud'));               
            }
        }

        public function delete($id){
            $this->crud_model->delete('users',$id);
            redirect(base_url('/index.php/crud/index'));
        }
    }
?>