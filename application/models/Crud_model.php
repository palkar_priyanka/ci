<?php
    class Crud_model extends CI_model{

        public function show_all(){
            $result = $this->db->get('users');
            return $result->result();
        }

        public function insert_data($table,$data){
            $this->db->insert($table, $data);
            if($this->db->insert_id()){
                return true;
            }else{
                return false;
            }
        }

        public function delete($table,$id){
            $this->db->delete($table,['id'=>$id]);
        }

        public function user_get_by_id($table,$id){
            $result = $this->db->get_where($table,array('id'=>$id));
            return $result->row();
        }

        public function update($table,$id,$data){
            $this->db->where('id',$id);
            $this->db->update($table,$data);
        }
    }
?>