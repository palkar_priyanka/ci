<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
    <?php 
        echo form_open();
        echo form_label('Name:');
        $name = array(
            'name'          => 'name',
            'id'            => 'name'
        );
        echo form_input($name);
    ?>
    <br>
    <br>
    <?php
        echo form_label('Address:');
        $address = array(
            'name'          => 'address',
            'id'            => 'address'
        );
        echo form_input($address);
    ?>
    <br>
    <br>
    <?php
        echo form_label('Gender:');
        $gender = array(
            'name'          => 'gender',
            'id'            => 'gender'
        );
        echo form_input($gender);
    ?>
    <br>
    <br>
    <?php
        $submit = array(
            'name'          => 'submit',
            'id'            => 'submit',
            'value'         => 'submit'
        );
        echo form_submit($submit);
        echo form_close();
    ?>
</body>
</html>