<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <script src="main.js"></script>
</head>
<body>
    <?php 
        $attr = array(
            'method'            => 'POST'
        );
        echo form_open(base_url('index.php/crud/update'),$attr);
        echo form_label('Name:');
        $name = array(
            'name'          => 'name',
            'id'            => 'name',
            'value'        =>  $data->name
        );
        echo form_input($name);
    ?>
    <br>
    <input type="hidden" value="<?= $data->id; ?>" name="id">
    <br>
    <?php
        echo form_label('Address:');
        $address = array(
            'name'          => 'address',
            'id'            => 'address',
            'value'         =>  $data->address
        );
        echo form_input($address);
    ?>
    <br>
    <br>
    <?php
        echo form_label('Gender:');
        $gender = array(
            'name'          => 'gender',
            'id'            => 'gender',
            'value'         =>  $data->gender
        );
        echo form_input($gender);
    ?>
    <br>
    <br>
    <?php
        $submit = array(
            'name'          => 'submit',
            'id'            => 'submit',
            'value'         => 'update'
        );
        echo form_submit($submit);
        echo form_close();
    ?>
</body>
</html>