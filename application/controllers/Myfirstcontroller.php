<?php
class Myfirstcontroller extends CI_controller{
    
    public function index(){
        echo "I am in myfirstcontroller.";
    }

    public function myview(){
        $this->load->view('Myfirstview');
    }

    public function show_data(){
        $users = [
            array(
                'Name' => 'priyanka',
                'address' => 'Ghansoli',
                'contact' => '9856213546',
                'salary' => '10000'
            ),
            array(
                'Name' => 'Ritesh',
                'address' => 'Vikhroli',
                'contact' => '6523458762',
                'salary' => '15000'
            ),
            array(
                'Name' => 'Amit',
                'address' => 'Sury Nagar',
                'contact' => '5684213568',
                'salary' => '20000'
            ),
        ];
        $data['user'] = $users;
        $data['priya'] = 'I am in Myfirstcontroller and in show method.';
        $data['message'] = "I am in Show_data method from Myfirstcontroller!!!";
        $this->load->view('Myfirstview',$data);

    }

    public function area_circle($radius){
        // print $radius;
        // print $a;
        // $area = 3.14 * $radius * $radius;
        $data['radius'] = $radius;
        $this->load->view('Areaofcircle',$data);
    }
}
?>